package com.example.actividad5firebasebrahyan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.lang.Exception
import java.lang.NullPointerException
import kotlin.concurrent.thread

class ActividadPrincipal : AppCompatActivity() {

    private var TAG:String = "ActividadPrincipal"
    private var auth =Firebase.auth.currentUser
    private var db = Firebase.firestore

    val miUsuario:Profiles = Profiles("","","","")

    lateinit var tvUserName:TextView
    lateinit var tvEmail:TextView
    lateinit var tvPassword:TextView
    lateinit var tvUid:TextView
    lateinit var btnShowData:Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actividad_principal)

        tvUserName=this.findViewById(R.id.tvUserName)
        tvEmail=this.findViewById(R.id.tvEmail)
        tvPassword=this.findViewById(R.id.tvPassword)
        tvUid=this.findViewById(R.id.tvUid)
        btnShowData=this.findViewById(R.id.btnShowData)

        btnShowData.setOnClickListener{

            this.showPersonalData()
        }


    }

    private fun getPersonalData(){



        val docRef = db.collection("Perfiles").document(auth!!.uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {

                    Log.d(TAG, "DocumentSnapshot data: ${document.data}")
                    miUsuario.Emai=document.getString("Email")!!
                    miUsuario.Usernam=document.getString("UserName")!!
                    miUsuario.passwor=document.getString("Password")!!
                    miUsuario.UI=auth!!.uid
                } else {

                    Log.d(TAG, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Toast.makeText(this,"Email",Toast.LENGTH_LONG).show()
                Log.d(TAG, "get failed with ", exception)
            }



    }

    private fun showPersonalData(){

        getPersonalData()


        tvUserName.text= miUsuario.Usernam
        tvEmail.text= miUsuario.Emai
        tvUid.text= miUsuario.UI
        tvPassword.text= miUsuario.passwor


    }



}