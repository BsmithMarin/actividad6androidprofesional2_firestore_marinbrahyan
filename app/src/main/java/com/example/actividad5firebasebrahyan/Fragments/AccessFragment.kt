package com.example.actividad5firebasebrahyan.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.actividad5firebasebrahyan.R
import com.google.firebase.auth.FirebaseAuth



class AccessFragment(listener: View.OnClickListener) : Fragment() {

    var btnLoginAccess:Button?=null
    var btnSingUpAccess:Button?=null

    private var listener:View.OnClickListener? = listener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_access, container, false)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btnLoginAccess=view.findViewById(R.id.btnLoginAccess)
        btnSingUpAccess=view.findViewById(R.id.btnSingUpAccess)

        btnLoginAccess!!.setOnClickListener(listener)
        btnSingUpAccess!!.setOnClickListener(listener)


    }




}